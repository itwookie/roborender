#ifndef __MESH_H__
#define __MESH_H__

#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <stdexcept>

#include "Polygon.hpp"
#include "PolyGroup.hpp"
#include "Vertex.hpp"
#include "Material.hpp"
#include "MeshTypes.hpp"

class Mesh {
private:
    std::vector<PolyGroup*> g{};
    std::vector<Vector3f*> vertices{};
    std::vector<Vector3f*> vertex_normals{}; //can be used for smoothing
    std::vector<Vector2f*> vertex_uv{}; //texture points
    std::map<std::string, Material*> m{};
public:
    inline Mesh(){
        Material* invalidMaterial = new Material();
        m.emplace("__INVALID", invalidMaterial);
        PolyGroup* unnamedGroup = new PolyGroup();
        g.push_back(unnamedGroup);
    }
    inline ~Mesh(){
        for (auto it = m.begin(); it != m.end(); it++) { delete (it->second); }
        for (auto it = g.begin(); it < g.end(); it++) { delete (*it); }
        for (auto it = vertices.begin(); it < vertices.end(); it++) { delete (*it); }
        for (auto it = vertex_normals.begin(); it < vertex_normals.end(); it++) { delete (*it); }
        for (auto it = vertex_uv.begin(); it < vertex_uv.end(); it++) { delete (*it); }

    }

    /** @return false if this method failed because the material is already registered */
    inline bool addMaterial(std::string& name, Material* material) {
        if (m.find(name) != m.end()) {
            std::cerr << "Double material registration: " << name << std::endl;
            return false;
        }
        std::string key(name);
        m.emplace(key, material);
        return true;
    }

    inline Material* getMaterial(std::string& name) {
        auto it = m.find(name);
        if (it == m.end()) {
            std::cerr << "Invaild material " << name << std::endl;
            it = m.find("__INVALID");
        }
        return (it->second);
    }

    inline PolyGroup* createPolyGroup(std::string& name) {
        PolyGroup* newGroup = new PolyGroup(name);
        g.push_back(newGroup);
        return newGroup;
    }

    inline PolyGroup* getPolyGroup(size_t n) {
        if (n >= g.size()) {
            std::stringstream sb;
            sb << "Group index out of bounds: "<<n<<"<"<<g.size()<<std::endl;
            throw std::runtime_error(sb.str());
        }
        return g[n];
    }

    inline void addVertex(float x, float y, float z) {
        Vector3f* v = new Vector3f{x,y,z};
        vertices.push_back(v);
    }
    inline void addVertexNormal(float n, float m, float o) {
        Vector3f* vn = new Vector3f{n,m,o};
        vertex_normals.push_back(vn);
    }
    inline void addVertexUV(float u, float v) {
        Vector2f* vt = new Vector2f{u,v};
        vertex_uv.push_back(vt);
    }
    inline Vector3f* getVertex(size_t n) {
        if (n >= vertices.size()) {
            std::stringstream sb;
            sb << "Vertices index out of bounds: "<<n<<"<"<<vertices.size()<<std::endl;
            throw std::runtime_error(sb.str());
        }
        return vertices[n];
    }
    inline Vector3f* getVertexNormal(size_t n) {
        if (n >= vertex_normals.size()) {
            std::stringstream sb;
            sb << "Normals index out of bounds: "<<n<<"<"<<vertex_normals.size()<<std::endl;
            throw std::runtime_error(sb.str());
        }
        return vertex_normals[n];
    }
    inline Vector2f* getVertexUV(size_t n) {
        if (n >= vertex_uv.size()) {
            std::stringstream sb;
            sb << "Textures index out of bounds: "<<n<<"<"<<vertex_uv.size()<<std::endl;
            throw std::runtime_error(sb.str());
        }
        return vertex_uv[n];
    }

    void render();
};

#endif // __MESH_H__

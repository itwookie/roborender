#ifndef __MESHTYPES_HPP__
#define __MESHTYPES_HPP__

typedef struct {
    double x;
    double y;
    double z;
} Vector3d;
typedef struct {
    double x;
    double y;
} Vector2d;
typedef struct {
    float x;
    float y;
    float z;
} Vector3f;
typedef struct {
    float x;
    float y;
} Vector2f;

#endif // __MESHTYPES_HPP

#ifndef __FS_UTIL_H__
#define __FS_UTIL_H__

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

void fsu_split(std::vector<std::string>& result, const std::string& target, const std::string& delimiter);
void fsu_join(std::string& result, std::vector<std::string>& parts, const std::string& glue);

std::string fsu_getParent(const std::string& path);
std::string fsu_append(const std::string& base, const std::string& another);

#endif // __FS_UTIL_H__

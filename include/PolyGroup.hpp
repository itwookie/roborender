#ifndef __POLYGROUP_H__
#define __POLYGROUP_H__

#include "Polygon.hpp"
#include <glad/glad.h>
#include <vector>
#include <string>

class PolyGroup {
private:
    std::vector<Triangle*> p{};
    std::string name;
    bool isDirty=true;

    GLuint vboP=0, vboN=0, vboC=0, vboT=0, vao=0;
public:
    inline PolyGroup(){
        name = "__WORLD";
    }
    inline PolyGroup(const std::string& aname){
        name = aname;
        std::cout << "PolyGroup " << this->name << " constructed" << std::endl;
    }
    inline ~PolyGroup() {
        std::cout << "PolyGroup " << this->name << " destructed" << std::endl;
        for (auto it = p.begin(); it < p.end(); it++) { delete *it; }

        if (vao!=0) glDeleteBuffers(1, &vao);
        if (vboP!=0) glDeleteBuffers(1, &vboP);
        if (vboN!=0) glDeleteBuffers(1, &vboN);
        if (vboC!=0) glDeleteBuffers(1, &vboC);
        if (vboT!=0) glDeleteBuffers(1, &vboT);
    }
    inline void addTriangle(Triangle* triangle) {
        p.push_back(triangle);
    }
    inline void markForRedraw() { isDirty = true; }
    inline Material* getFirstMaterial() {
        if (p.size() == 0) return nullptr;
        return (*p.begin())->material;
    }
    void createVertexBuffer();
    void render();
};

#endif // __POLYGROUP_H__

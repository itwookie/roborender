#ifndef __MESHLOADER_H__
#define __MESHLOADER_H__

#include "Mesh.hpp"
#include <fstream>
#include <fs_util.hpp>
#include <stdexcept>

void loadMeshOBJ(Mesh** mesh, const char* objFile);

#endif // __MESHLOADER_H__

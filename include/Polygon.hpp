#ifndef __POLYGON_H__
#define __POLYGON_H__

#include "MeshTypes.hpp"
#include "Material.hpp"

class Triangle {
private:
    bool useNormals;
    Vector3f *point[3];
    Vector2f *uv[3];
    Vector3f *normal[3];
public:
    Material *material;
    inline Triangle(Material* mat, Vector3f* v1, Vector2f* t1, Vector3f* n1, Vector3f* v2, Vector2f* t2, Vector3f* n2, Vector3f* v3, Vector2f* t3, Vector3f* n3) {
        point[0] = v1; point[1] = v2; point[2] = v3;
        uv[0] = t1; uv[1] = t2; uv[2] = t3;
        normal[0] = n1; normal[1] = n2; normal[2] = n3;
        useNormals = true;
        material = mat;
    }
    inline Triangle(Material* mat, Vector3f* v1, Vector2f* t1, Vector3f* v2, Vector2f* t2, Vector3f* v3, Vector2f* t3) {
        point[0] = v1; point[1] = v2; point[2] = v3;
        uv[0] = t1; uv[1] = t2; uv[2] = t3;
        useNormals = false;
        material = mat;
    }
    inline ~Triangle(){} //values are deleted by polygroup

    //void writeVertexBuffer(float* bufferPoint);
    void writeVertexPoints(GLfloat* buffer);
    void writeVertexNormals(GLfloat* buffer);
    void writeVertexColors(GLfloat* buffer);
    void writeVertexTexCoords(GLfloat* buffer);
    void render();
};

#endif // __POLYGON_H__

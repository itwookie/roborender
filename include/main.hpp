#ifndef __MAIN_HPP__
#define __MAIN_HPP__

#include <windows.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//#include <armadillo>
#include <stdexcept>

#include "Mesh.hpp"
#include "MeshLoader.hpp"
#include "Shader.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void glfw_error_callback(int error, const char* description);
void processInput();
void main_render();
int _renderInit();
int _renderStart();

#endif // __MAIN_HPP__

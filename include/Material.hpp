#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <windows.h>
#include <glad/glad.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <png.h>

class Material {
private:
    float color[3]={1.0f, 0.0f, 1.0f}; // Kd
    std::string tex; //map_Kd texture path
    png_image image;
    png_bytep buffer;
    bool isColor=true;
    GLuint hdlTexture;

public:
    inline Material() {
    }
    inline void setTexture(const std::string& mapName) {
        std::cout << "[Material] Loading " << mapName << std::endl;
        DWORD attr = GetFileAttributes(mapName.c_str());
        //if (INVALID_FILE_ATTRIBUTES == GetFileAttributes(mapName.c_str()) && GetLastError()==ERROR_FILE_NOT_FOUND) {
        if (attr == INVALID_FILE_ATTRIBUTES || (attr & FILE_ATTRIBUTE_DIRECTORY)) {
            std::cerr << "Could not find file: " << mapName << std::endl;
        } else {
            isColor = false;
            tex.assign(mapName);

            memset(&image, 0, (sizeof image));
            image.version = PNG_IMAGE_VERSION;
            if (png_image_begin_read_from_file(&image, tex.c_str())) {
                image.format = PNG_FORMAT_RGBA;
                buffer = (png_bytep)malloc(PNG_IMAGE_SIZE(image));
                if (buffer != NULL &&
                    png_image_finish_read(&image, NULL/*background*/, buffer, 0/*row_stride*/, NULL/*colormap*/) != 0)
                {
                    glGenTextures(1, &hdlTexture);
                    glBindTexture(GL_TEXTURE_2D, hdlTexture);
                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.height, image.width, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

                    free(buffer);

                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    //glGenerateMipmap(GL_TEXTURE_2D);
                } else {
                    std::cerr << "Could not load texture: " << tex << std::endl;
                }
            } else {
                png_image_free(&image);
                std::cerr << "Unsupported Texture: " << tex << std::endl;
                isColor = true;
            }
        }
    }
    inline void setColor(float r, float g, float b) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
    inline void setColor(float gray) {
        color[0] = color[1] = color[2] = gray;
    }
    inline void setColor(int r, int g, int b) {
        color[0] = (float)r/255.0;
        color[1] = (float)g/255.0;
        color[2] = (float)b/255.0;
    }
    inline ~Material(){
        if (!isColor) {
            png_image_free(&image);
            glDeleteTextures(1, &hdlTexture);
        }
    }
    inline getColor(float* writeAt) {
        writeAt[0] = color[0];
        writeAt[1] = color[1];
        writeAt[2] = color[2];
    }
    inline bool isTexture() { return !isColor; }
    void use();
};

#endif // __MATERIAL_H__

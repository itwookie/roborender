# RoboRendre

Supposed to provide a industrial robot visualisation based
on DH parameters and Wavefront OBJ for Mathematic tools like
Matlab.

Currently used libraries:
* opengl32
* glu32
* gdi32 (?)
* glfw3
* png
  * zlib (?)
* armadillo
  * openblas
  * lapack
  * hdf5
Additionally uses glad.
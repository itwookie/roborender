#version 330 compatibility

uniform sampler2D texture;

varying vec4 diffuse;
varying vec4 specular;

void main() {
  vec4 texColor = texture2D(texture, gl_TexCoord[0].st);
  gl_FragColor = gl_LightSource[0].ambient +
                 (diffuse * vec4(texColor.rgb, 1.0)) + (specular * texColor.a);
}
#version 330 compatibility

varying vec4 diffuse;
varying vec4 specular;

uniform vec3 CAMERA_POSITION;

void main() {
  gl_Position = ftransform();
  gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
  vec3 normal = gl_Normal;
  vec3 lightVector = gl_LightSource[0].position.xyz - gl_Vertex.xyz;
  float dist = length(lightVector);
  float attenuation =
      1.0 / (gl_LightSource[0].constantAttenuation +
             gl_LightSource[0].linearAttenuation * dist +
             gl_LightSource[0].quadraticAttenuation * dist * dist);
  lightVector = normalize(lightVector);
  float nxDir = max(0.0, dot(normal, lightVector));
  diffuse = gl_LightSource[0].diffuse * nxDir * attenuation;
  //-----------------NEW CODE TO COMPUTE SPECULAR TERM----------------
  //-----------------NEW CODE TO COMPUTE SPECULAR TERM----------------
  float specularPower = 0.0;
  if (nxDir != 0.0) {
    // Programatic way
    // vec3 cameraPosition = vec3(gl_ModelViewMatrixInverse * vec4(0,0,0,1.0));
    // cameraPosition = normalize(cameraPosition - gl_Vertex.xyz);
    vec3 cameraVector = normalize(CAMERA_POSITION - gl_Vertex.xyz);
    vec3 halfVector = normalize(lightVector + cameraVector);
    float nxHalf = max(0.0, dot(normal, halfVector));
    specularPower = pow(nxHalf, gl_FrontMaterial.shininess);
  }
  specular = gl_LightSource[0].specular * specularPower * attenuation;
}
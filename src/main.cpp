#include "main.hpp"

#define PROJECT_WINDOW_CLASS_NAME "RobotRender"
#define PROJECT_WINDOW_TITLE "Robot Render"
#define PROJECT_WINDOW_DEFAULT_WIDTH 720
#define PROJECT_WINDOW_DEFAULT_HEIGHT 720

#define toRad (3.141592/180.0)

//LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
//void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
//void DisableOpenGL(HWND, HDC, HGLRC);

int showWindow();

//void camera(arma::vecd& campos, arma::vecd& lookat) {
//    arma::vecd dir = (lookat-campos).normalise();
//    arma::matd a={ { 1.0,    0.0,    0.0,   0.0 }, //right
//                     0.0,    0.0,    1.0,   0.0 }, //up
//                    dir[0], dir[1], dir[2], 0.0 },
//                     0.0,    0.0,    0.0,   1.0 } };
//    arma::matd t(4,4,arma::fill::eye);
//    t.col(3) = {-campos[0], -campos[1], -campos[2], 1.0};
//    arma::matd res = a*t;
//    std::cout << res;
//}

Mesh* mesh=nullptr;
GLFWwindow* window;
Shader* shader=nullptr;
GLint mvp_location, vpos_location, vcol_location;
Material* splashMat;
GLfloat light0pos[] = {0.0,1.0,0.0,0.0}; //y = up? w=0->directional

int main(int argc, const char** argv) {
    int ret = _renderInit();

    if (ret == 0) {
        try {
            loadMeshOBJ(&mesh, "../example/iiwa7 R800 - MF Touch pneumatisch LowPoly.obj");
        } catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
            return EXIT_FAILURE;
        }

        ret = _renderStart();
    }

    Sleep(5000);
    exit(0);
    if (mesh != nullptr) delete mesh;
    if (shader != nullptr) delete shader;
    return ret;
}

int _renderInit() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwSetErrorCallback(glfw_error_callback);

    window = glfwCreateWindow(PROJECT_WINDOW_DEFAULT_WIDTH, PROJECT_WINDOW_DEFAULT_HEIGHT, PROJECT_WINDOW_TITLE, NULL, NULL);
    if (window == NULL)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    shader = new Shader("default.vsh", "default.fsh");
    if (shader->hasErrors) return -1;

    framebuffer_size_callback(window, PROJECT_WINDOW_DEFAULT_WIDTH, PROJECT_WINDOW_DEFAULT_HEIGHT);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    printf("OpenGL version supported by this platform (%s): \n", glGetString(GL_VERSION));

    //last preparations

    glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LESS);
	glClearColor (0.2f, 0.2f, 0.3f, 1.0f);

//	glEnable (GL_CULL_FACE);
//	glCullFace (GL_BACK);
//	glFrontFace (GL_CCW);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable (GL_LIGHTING);
	glEnable (GL_LIGHT0);

    return 0;
}
int _renderStart() {

    GLfloat camPos[] = {0.0,0.0,-500.0};

    while(!glfwWindowShouldClose(window))
    {
        processInput();

//        glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glPushMatrix();
        glLoadIdentity();
        glLightfv(GL_LIGHT0, GL_POSITION, light0pos); //from +y? is y up?
        glPopMatrix();

        glPushMatrix();
        glScalef(0.001,0.001,0.001);
        glRotatef(90.0, 1.0, 0.0, 0.0); //model hoch drehen
        glTranslatef(camPos[0], camPos[1], camPos[2]);

//        glEnable(GL_TEXTURE_2D);
//        glMatrixMode(GL_MODELVIEW);

        shader->use();
        shader->setVector3f("CAMERA_POSITION", camPos);
        mesh->render();
        glPopMatrix();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
//    int off;
//    if (width < height) {
//        off = (height-width)/2.0;
//        glViewport(0,off,width,width);
//    } else {
//        off = (width-height)/2.0;
//        glViewport(off,0,height,height);
//    }

    float ratio = width / (float) height;
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);

//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluPerspective(45.0, (float)w / (float)h, 0.01f, 100.0f);
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
}

void glfw_error_callback(int error, const char* description) {
    std::cerr << "GLFE Error " << error << std::endl << description << std::endl;
}

void processInput()
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

inline void main_render() {

}

#include "Material.hpp"

void Material::use() {
    //std::cout << "Use material " << tex << std::endl;
    if (isColor) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glColor3f( color[0], color[1], color[2] );
    } else {
        glBindTexture(GL_TEXTURE_2D, hdlTexture);
    }
}

#include "fs_util.hpp"

void fsu_split(std::vector<std::string>& result, const std::string& target, const std::string& delimiter) {
    size_t from = 0;
    size_t index = -1;
    while ((index = target.find(delimiter, from)) != std::string::npos) {
        std::string part = target.substr(from, index-from);
        result.push_back(part);
        from = index+1;
    }
    if (from < target.size()) {
        std::string lastpart = target.substr(from, target.size());
        result.push_back(lastpart);
    }
}
void fsu_join(std::string& result, std::vector<std::string>& parts, const std::string& glue) {
    if (parts.size() == 0) result.assign("");
    std::stringstream sb;
    sb << parts[0];
    for (size_t i = 1; i < parts.size(); i++) {
        sb << glue << parts[i];
    }
    result.assign(sb.str());
}

std::string fsu_getParent(const std::string& path) {
    size_t iat = path.find_last_of('/');
    if (iat == std::string::npos) {
        std::cout << "cannot find / in " << path << std::endl;
        iat = path.size();
    }
    std::string result = path.substr(0,iat);
    return result;
}
std::string fsu_append(const std::string& base, const std::string& another) {
    std::string result(base);
    std::vector<std::string> parts;
    fsu_split(parts, another, "/");
    for (auto it = parts.begin(); it < parts.end(); it++) {
        if (*it == "..") {
            std::string tmp = fsu_getParent(result);
            result.assign(tmp);
        } else if (*it == ".") {
            //ignore
        } else {
            std::stringstream sb;
            sb << result;
            if (result[result.size()-1] != '/') {
                sb << '/';
            }
            sb << *it;
            result.assign(sb.str());
        }
    }
    return result;
}

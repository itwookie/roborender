#include "MeshLoader.hpp"

void __skipSpace(std::ifstream& S) { while (isspace(S.peek())) S.ignore(1); }
void __loadMaterialsMTL(Mesh* mesh, const char* mtlFile);
void __getLine(std::istream& stream, std::string& target) {
    std::stringstream sb;
    for (char c=stream.peek(); c != '\r' && c != '\n'; c=stream.peek()) {
        stream.ignore(1);
        sb << c;
    }
    if (!stream.eof() && stream.good()) {
        for (char c=stream.peek(); c == '\r' || c != '\n'; c=stream.peek()) {
            stream.ignore(1);
        }
    }
    target.assign(sb.str());
}

void loadMeshOBJ(Mesh** mesh, const char* objFile) {
    std::cout << "[OBJ] Loading " << objFile << std::endl;

    *mesh = new Mesh();

    std::ifstream obj(objFile, std::istream::in);
    std::string tmpString;
    std::vector<std::string> unsupported{};

    PolyGroup* active_g = (*mesh)->getPolyGroup(0);
    Material* active_mtl;

    while (!obj.eof() && obj.good()) {
        __skipSpace(obj);
        if (obj.eof() || !obj.good()) break;
        if (obj.peek() == '#') {
            //echo comment
            __skipSpace(obj);
            __getLine(obj, tmpString);
//            std::cout << "[OBJ] Comment: " << tmpString << std::endl;
            continue;
        }
        obj >> tmpString;
        if (tmpString == "mtllib") {
            std::cout << "mtllib" << std::endl;
            __skipSpace(obj);
            __getLine(obj, tmpString);
            std::string dir = fsu_getParent(objFile);
            tmpString = fsu_append(dir, tmpString);
            __loadMaterialsMTL(*mesh, tmpString.c_str());
        } else if (tmpString == "v") {
            float x,y,z;
            obj >> x >> y >> z;
            (*mesh)->addVertex(x,y,z);
            __getLine(obj, tmpString); //skip line end
        } else if (tmpString == "vn") {
            float n,m,o;
            obj >> n >> m >> o;
            (*mesh)->addVertexNormal(n,m,o);
            __getLine(obj, tmpString); //skip line end
        } else if (tmpString == "vt") {
            float u,v;
            obj >> u >> v;
            (*mesh)->addVertexUV(u,v);
            __getLine(obj, tmpString); //skip line end
        } else if (tmpString == "g") {
            __skipSpace(obj);
            __getLine(obj, tmpString);
            active_g = (*mesh)->createPolyGroup(tmpString);
            std::cout << "[OBJ] Group: " << tmpString << std::endl;
        } else if (tmpString == "usemtl") {
            __skipSpace(obj);
            __getLine(obj, tmpString); //read material name
            active_mtl = (*mesh)->getMaterial(tmpString);
        } else if (tmpString == "f") {
            Vector3f *point_vert[3]; //get data from extern
            Vector2f *point_uv[3];
            Vector3f *point_norm[3];

            size_t v,t,n;
            bool hasN=false;
            for (int i=0; i<3; i++) {
                __skipSpace(obj);
                obj >> v;
                if (obj.peek() == '/') {
                    obj.ignore(1);
                    if (obj.peek() == '/') {
                        throw std::runtime_error("Unsupported OBJ file - material is required on faces!");
                    }
                    obj >> t;
                    if (obj.peek() == '/') {
                        obj.ignore(1);
                        obj >> n;
                        hasN = true;
                    }
                } else {
                    throw std::runtime_error("Unsupported OBJ file - material is required on faces!");
                }
                if (hasN) {
                    point_vert[i] = (*mesh)->getVertex(v-1);
                    point_uv[i] = (*mesh)->getVertexUV(t-1);
                    point_norm[i] = (*mesh)->getVertexNormal(n-1);
                } else {
                    point_vert[i] = (*mesh)->getVertex(v-1);
                    point_uv[i] = (*mesh)->getVertexUV(t-1);
                }
            }
            //construct polygon
            Triangle* tri;
            if (hasN) {
                tri = new Triangle( active_mtl,
                        point_vert[0], point_uv[0], point_norm[0],
                        point_vert[1], point_uv[1], point_norm[1],
                        point_vert[2], point_uv[2], point_norm[2]
                );
            } else {
                tri = new Triangle( active_mtl,
                        point_vert[0], point_uv[0],
                        point_vert[1], point_uv[1],
                        point_vert[2], point_uv[2]
                );
            }
            __getLine(obj, tmpString); //skip line end
            active_g->addTriangle(tri);
        } else {
            std::cerr << "[OBJ] Unsupported operator: " << tmpString << std::endl;
        }
    }

    obj.close();
    std::cout << "[OBJ] DONE " << objFile << std::endl;
}

void __loadMaterialsMTL(Mesh* mesh, const char* mtlFile) {
    std::cout << "[MTL] Loading " << mtlFile << std::endl;

    std::ifstream mtl(mtlFile, std::istream::in);
    std::string tmpString;

    std::string baseDir = fsu_getParent(mtlFile);
    baseDir = fsu_append(baseDir, "Textures"); //we keep textures here (not speced)

    std::string matName;
    Material* mat = nullptr;

    while (!mtl.eof() && mtl.good()) {
        __skipSpace(mtl);
        if (mtl.peek() == '#') {
            //echo comment
            __getLine(mtl, tmpString);
//            std::cout << "[MTL] Comment: " << tmpString << std::endl;
            continue;
        }
        mtl >> tmpString;
        if (tmpString == "newmtl") {
            __skipSpace(mtl);
            __getLine(mtl, tmpString);
            if (mat != nullptr) {
                mesh->addMaterial(matName, mat);
            }
            std::cout << "[MTL] New material: " << tmpString << std::endl;
            matName.assign(tmpString);
            mat = new Material();
        } else if (tmpString == "Kd") {
            std::cout << "[MTL] Color" << std::endl;
            __skipSpace(mtl);
            float r,g,b;
            mtl >> r >> g >> b;
            __getLine(mtl,tmpString);//skip line end
            mat->setColor(r,g,b);
        } else if (tmpString == "map_Kd") {
            std::cout << "[MTL] Texture mapped" << std::endl;
            __skipSpace(mtl);
            __getLine(mtl, tmpString);
            tmpString = fsu_append(baseDir, tmpString);
            mat->setTexture(tmpString);
        } else if (!mtl.eof() && mtl.good()) {
            __getLine(mtl, tmpString); //skip line
        }
    }

    if (mat != nullptr)
        mesh->addMaterial(matName, mat);

    mtl.close();
    std::cout << "[MTL] DONE " << mtlFile << std::endl;
}

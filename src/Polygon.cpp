#include "Polygon.hpp"

void Triangle::writeVertexPoints(GLfloat* buffer) {
    for (int i=0; i<3; i++) {
        *(buffer+i*3+0) = point[i]->x;
        *(buffer+i*3+1) = point[i]->y;
        *(buffer+i*3+2) = point[i]->z;
    }
}
void Triangle::writeVertexNormals(GLfloat* buffer) {
    for (int i=0; i<3; i++) {
        *(buffer+i*3+0) = normal[i]->x;
        *(buffer+i*3+1) = normal[i]->y;
        *(buffer+i*3+2) = normal[i]->z;
    }
}
void Triangle::writeVertexColors(GLfloat* buffer) {
    for (int i=0; i<3; i++) {
        material->getColor(buffer+i*3);
    }
}
void Triangle::writeVertexTexCoords(GLfloat* buffer) {
    for (int i=0; i<3; i++) {
        if (material->isTexture()) {
            *(buffer+i*2+0) = uv[i]->x;
            *(buffer+i*2+1) = uv[i]->y;
        } else {
            *(buffer+i*2+0) = 0.0;
            *(buffer+i*2+1) = 0.0;
        }
    }
}

void Triangle::render() {

    glBegin(GL_TRIANGLES);

    material->use();

    if (useNormals) glNormal3d( normal[0]->x, normal[0]->y, normal[0]->z );
    glTexCoord2d( uv[0]->x, uv[0]->y );
    glVertex3d( point[0]->x,  point[0]->y, point[0]->z );

    if (useNormals) glNormal3d( normal[1]->x, normal[1]->y, normal[1]->z );    glTexCoord2d( uv[1]->x, uv[1]->y );
    glVertex3d( point[1]->x,  point[1]->y, point[1]->z );

    if (useNormals) glNormal3d( normal[2]->x, normal[2]->y, normal[2]->z );
    glTexCoord2d( uv[2]->x, uv[2]->y );
    glVertex3d( point[2]->x,  point[2]->y, point[2]->z );

    glEnd();

}

#include "PolyGroup.hpp"

void PolyGroup::createVertexBuffer() {
    std::cout << "Refreshing Vertex Buffer for " << this->name << " with " << p.size() << " triangles" << std::endl;
    isDirty = false;

    if (vao!=0) { glDeleteVertexArrays(1, &vao); vao = 0; }
    if (vboP!=0) { glDeleteBuffers(1, &vboP); vboP = 0; }
    if (vboN!=0) { glDeleteBuffers(1, &vboN); vboN = 0; }
    if (vboC!=0) { glDeleteBuffers(1, &vboC); vboC = 0; }
    if (vboT!=0) { glDeleteBuffers(1, &vboT); vboT = 0; }

    if (p.size()==0) return;

    size_t points = p.size()*3; //3 points per triangle
    //float* buffer all values
    GLfloat* bufP = new GLfloat[points*3]; // 3 coordinates per point
    GLfloat* bufN = new GLfloat[points*3]; // 3 coordinates per point
    GLfloat* bufC = new GLfloat[points*3]; // 3 coordinates per point
    GLfloat* bufT = new GLfloat[points*2]; // 2 coordinates per point
    GLfloat *ptrP = bufP, *ptrN = bufN, *ptrC = bufC, *ptrT = bufT;
    for (auto it = p.begin(); it < p.end(); it++) {
        (*it)-> writeVertexPoints   (ptrP); ptrP += 9;//writes 3 points * 3 coordinates
        (*it)-> writeVertexNormals  (ptrN); ptrN += 9;//writes 3 points * 3 coordinates
        (*it)-> writeVertexColors   (ptrC); ptrC += 9;//writes 3 points * 3 coordinates
        (*it)-> writeVertexTexCoords(ptrT); ptrT += 6;//writes 3 points * 2 coordinates
    }

    //move data to GPU RAM

    glGenBuffers(1, &vboP);
    glBindBuffer(GL_ARRAY_BUFFER, vboP);
    glBufferData(GL_ARRAY_BUFFER, points*3*sizeof(GLfloat), bufP, GL_STATIC_DRAW);

    glGenBuffers(1, &vboN);
    glBindBuffer(GL_ARRAY_BUFFER, vboN);
    glBufferData(GL_ARRAY_BUFFER, points*3*sizeof(GLfloat), bufN, GL_STATIC_DRAW);

    glGenBuffers(1, &vboC);
    glBindBuffer(GL_ARRAY_BUFFER, vboC);
    glBufferData(GL_ARRAY_BUFFER, points*3*sizeof(GLfloat), bufC, GL_STATIC_DRAW);

    glGenBuffers(1, &vboT);
    glBindBuffer(GL_ARRAY_BUFFER, vboT);
    glBufferData(GL_ARRAY_BUFFER, points*2*sizeof(GLfloat), bufT, GL_STATIC_DRAW);

    //tell memory structure

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vboP);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, vboN);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, vboC);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, vboT);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    //free temporaries

    delete(bufP);
    delete(bufN);
    delete(bufC);
    delete(bufT);
}
void PolyGroup::render() {
    if (isDirty) {
        createVertexBuffer();
    }
    if (vao == 0) return; //no data to render

    Material* mat = getFirstMaterial();
    if (mat!=nullptr) getFirstMaterial()->use();

    glBindVertexArray (vao);
    glDrawArrays (GL_TRIANGLES, 0, p.size()*3);
}

#undef stride
